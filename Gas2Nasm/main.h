﻿#if defined(_WIN32) || defined(_WIN64)
#define _CRT_SECURE_NO_WARNINGS//For Visual studio
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef unsigned char UCHAR;
typedef unsigned int UINT;
UCHAR *dest0_, *dest1_;

typedef struct stack_alloc {
	UCHAR ibuf[8 * 1024 * 1024];
	UCHAR obuf[8 * 1024 * 1024];
} stack_alloc;


#define	NL			"\n"
#define	LEN_NL		1
#define GOLD_exit		exit


//gas2nasm.c
void output(UINT l, UCHAR *s);
void msgout0(int len, UCHAR *s);
unsigned int GO_strlen(const UCHAR *s);
int GOLD_write_t(const UCHAR *name, int len, const UCHAR *p0);
void msgout(UCHAR *s);
void errout(UCHAR *s);
void errout_s_NL(UCHAR *s, UCHAR *t);
UCHAR *readfile(UCHAR *name, UCHAR *b0, UCHAR *b1);

//g2nconv.c
UCHAR *skipspace(UCHAR *p);
UCHAR *getparam(UCHAR *p);
UCHAR *seek_token_end(UCHAR *s);
UCHAR *opcmp(UCHAR *q, UCHAR *s);
UCHAR *convmain(UCHAR *src0, UCHAR *src1, UCHAR *dest0, UCHAR *dest1);
UCHAR *checkparam(UCHAR *p);
void convparam(UCHAR *p, int i);
