﻿/*
		GAS2NASM x64対応版
*/

#include "main.h"


char errflag = 0;


int main(int argc, char **argv)
{
	stack_alloc *pwork;
	UCHAR *src1, i = 0;

	if (argc != 3) {
#if defined(_WIN32) || defined(_WIN64)
		msgout("Gas2Nasm" NL ">Gas2Nasm inputfile outputfile"NL);
#else
		msgout("Gas2Nasm" NL "$Gas2Nasm inputfile outputfile"NL);
#endif
		errout("Argument is wrong."NL);
	}
	pwork = (struct stack_alloc *) calloc(1, sizeof(struct stack_alloc));
	if (pwork == NULL){
		errout("calloc failed."NL);
	}
	src1 = readfile(argv[1], pwork->ibuf, pwork->ibuf + sizeof(pwork->ibuf));
	src1 = convmain(pwork->ibuf, src1, pwork->obuf, pwork->obuf + sizeof(pwork->obuf));
	if (src1 == NULL){
		errout("output filebuf over!" NL);
	}
	if (GOLD_write_t(argv[2], src1 - pwork->obuf, pwork->obuf)){
		errout_s_NL("can't write file: ", argv[2]);
	}

	return errflag;
}


UCHAR *readfile(UCHAR *name, UCHAR *b0, UCHAR *b1)
{
	FILE *fp;
	int bytes, len = b1 - b0;
	fp = fopen(name, "rb");
	if (fp == NULL)
		errout_s_NL("Can't open file: ", name);
	bytes = fread(b0, 1, len, fp);
	fclose(fp);
	if (len == bytes)
		errout("Input filebuf over!" NL);
	return b0 + bytes;
}

