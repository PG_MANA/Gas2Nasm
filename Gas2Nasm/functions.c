﻿//gas2nasm.cとg2nconv.cで使っている(というか基本関数みたいな)関数

#include "main.h"

void output(UINT l, UCHAR *s)
{
	if (l) {
		if (dest0_ + l >= dest1_)
			dest0_ = NULL;
		if (dest0_) {
			do {
				*dest0_++ = *s++;
			} while (--l);
		}
	}
	return;
}

void msgout0(int len, UCHAR *s)
{
	GOLD_write_t(NULL, len, s);
	return;
}

unsigned int GO_strlen(const UCHAR *s)
{
	const UCHAR *t = s;
	while (*s)
		s++;
	return s - t;
}


int GOLD_write_t(const UCHAR *name, int len, const UCHAR *p0)

{
	int ll = 0;
	FILE *fp = stdout;
	if (name) {
		fp = fopen(name, "w");
		if (fp == NULL)
			goto err;
	}
	if (len)
		ll = fwrite(p0, 1, len, fp);
	if (name)
		fclose(fp);
	if (ll != len)
		goto err;
	return 0;
err:
	return 1;
}

void msgout(UCHAR *s)
{
	GOLD_write_t(NULL, GO_strlen(s), s);
	return;
}

void errout(UCHAR *s)
{
	msgout(s);
	GOLD_exit(1);
}

void errout_s_NL(UCHAR *s, UCHAR *t)
{
	msgout(s);
	msgout(t);
	msgout(NL);
	GOLD_exit(1);
}

