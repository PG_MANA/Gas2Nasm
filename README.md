# Gas2Nasm
これはGCCの出力するGAS形式のファイルをNASM形式にするためのソフトです。
(Gas2Naskをいじっただけに近いですが...)
## ビルド環境
* Visual Studio 2013以降
* CMake 3.1 以上

## ビルド方法
### Visual Studioを使用する場合
Visual Studioを起動してGas2Nasm.slnを開いてビルドする。
### CMakeを使用する場合
```shell
cd Gas2Nasm
mkdir build
cd build
cmake ..
make
./gas2nasm
```

## 構成
```
.
├── CMakeLists.txt : CMake用設定ファイル(Visual Studio不使用)
├── Gas2Nasm
│   ├── Gas2Nasm.vcxproj : Visual Studio設定ファイル(CMakeでは不使用)
│   ├── Gas2Nasm.vcxproj.filters :  Visual Studio設定ファイル(CMakeでは不使用)
│   ├── functions.c : いろんな関数(ファイル出力関係)を集めたもの
│   ├── g2nconv.c : 変換システム本体
│   ├── gas2nasm.c : main関数、コマンドラインを受け取ってコンバーターに渡す。
│   ├── main.h : 関数宣言など
│   ├── main.rc : Visual Studio用リソースファイル(バージョン記載)(CMakeでは不使用)
│   └── resource.h : Visual Studio用リソースファイルヘッダ
├── Gas2Nasm.sln : Visual Studio設定ファイル(CMakeでは不使用)
├── LICENSE
└── README.md
```
## ライセンス
Copyright 2017 PG_MANA  
This software is Licensed under the Apache License Version 2.0  
See LICENSE.md  
## リンク
### 本ソフトウェアの説明ページ
 https://soft.taprix.org/product/gas2nasm.html
### 開発者のTwitterアカウント
  [https://twitter.com/PG_MANA_](https://twitter.com/PG_MANA_)
### 開発者のWebページ
  https://pg-mana.net
